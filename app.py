import json

from validate import Validation
from alert import Alerts
from mailchimp import MailChimp
from mailchimp_marketing.api_client import ApiClientError
from api import Api
        
#####
# This handles the older Webflow join forms
#         
def join_handler(event, _):
    alerts = Alerts()
    mailchimp = MailChimp()
    validation = Validation()

    print(event)
    data = json.loads(event["body"])
    print(data)
    
    # Mailchimp wants lowercase
    data["email"] = data["email"].lower()

    try:
        # Check phone/email validation API
        validation = Validation()
        print("Checking phone/email validation")

        print("Checking email validation")
        data["validation"] = {
            "email": validation.email(data["email"]),
            "phone": validation.phone(data["phone"]),
        }
        print(data["validation"])

        # Call mailchimp
        mailchimp.sign_up(data)
   
        return Api.lambda_resp(200, "calendly")
    except ApiClientError as error:
        # Handle errors from Mailchimp
        print("Error calling mailchimp: {}".format(error.text))
        alerts.email_alert("Error calling mailchimp: {0}\n{1}\n".format(error.text, data))
        return Api.lambda_resp(400, "thank-you")
