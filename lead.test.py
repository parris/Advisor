import os
os.environ["ENV"] = "preview"

import json
import copy

from lead import post_handler

def call_handler(l):
    # Flag as a test so that we don't post to mailchimp/dynamo
    l["is_test"] = True

    resp = post_handler({
        "body": json.dumps(l),
    }, None)

    if resp is True:
        return True
    
    return json.loads(resp.get("body", {}))

test_lead = {
    "email":       "parris.lead.test@gmail.com",
    "phone":       "8432132234",
    "first_name":  "parris",
    "last_name":   "test-lead",
    "age":         30,
    "income":      100000,
    "investments": 100000,
    "sub1":        "sub1 test string",
    "sub2":        "2",
    "sub3":        "!@#$%",
}

l = copy.deepcopy(test_lead)
l["email"] = "missing at symbol"
resp = call_handler(l)
assert resp.get("details")[0] == "Invalid email address", "Must reject emails w/o @ or ."

l = copy.deepcopy(test_lead)
l["email"] = "a@b.c"
resp = call_handler(l)
assert resp.get("details")[0] == "Email too short", "Must reject emails that are too short"

l = copy.deepcopy(test_lead)
l["phone"] = "118438675309"
resp = call_handler(l)
assert resp.get("details")[0] == "Incorrect phone number length", "Must reject phones that are too long"

l = copy.deepcopy(test_lead)
l["phone"] = "843867530"
resp = call_handler(l)
assert resp.get("details")[0] == "Incorrect phone number length", "Must reject phones that are too short"

l = copy.deepcopy(test_lead)
l["first_name"] = None
resp = call_handler(l)
assert resp.get("details")[0] == "First name cannot be empty", "Must reject empty first name"

l = copy.deepcopy(test_lead)
l["last_name"] = ""
resp = call_handler(l)
assert resp.get("details")[0] == "Last name cannot be empty", "Must reject empty last name"

l = copy.deepcopy(test_lead)
l["age"] = "letters"
resp = call_handler(l)
assert resp.get("details")[0] == "Age must be a number", "Age ain't nothing but a number"

l = copy.deepcopy(test_lead)
l["income"] = "letters"
resp = call_handler(l)
assert resp.get("details")[0] == "Income must be a number", "Must reject non-numeric income"

l = copy.deepcopy(test_lead)
l["investments"] = "letters"
resp = call_handler(l)
assert resp.get("details")[0] == "Investment must be a number", "Must reject non-numeric investments"

l = copy.deepcopy(test_lead)
l["income"] = None
l["investments"] = None
resp = call_handler(l)
assert len(resp.get("details")) >= 2, "Must return all errors at once"

l = copy.deepcopy(test_lead)
resp = call_handler(l)
assert resp == True, "Must exit True when no errors, and when it's a test"

print("Success (done)...")