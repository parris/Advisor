import { info } from 'console'
import { AdvisorPost } from './handler'
import { S3, STS } from 'aws-sdk'

export interface VanguardConf {
    AWS_ACCESS_KEY_ID: string
    AWS_SECRET_ACCESS_KEY: string
    VANGUARD_BUCKET: string
    VANGUARD_KMS_ARN: string
    VANGUARD_EXTERNAL_ID: string
    VANGUARD_ROLE_ARN: string
}

function vanguardTimestamp(): string {
    const date = new Date()

    const yyyy = date.getFullYear()
    const mm = String(date.getMonth() + 1).padStart(2, '0')
    const dd = String(date.getDate()).padStart(2, '0')
    const HH = String(date.getHours()).padStart(2, '0')
    const MM = String(date.getMinutes()).padStart(2, '0')
    const SS = String(date.getSeconds()).padStart(2, '0')

    return `${yyyy}-${mm}-${dd}-${HH}-${MM}-${SS}`
}

export async function post(advisorPost: AdvisorPost, conf: VanguardConf): Promise<any> {
    info('Posting to vanguard')

    // Create the one line CSV
    const headers = Object
        .keys(advisorPost)
        .join(',')

    const values = Object
        .values(advisorPost)
        .map(String)
        .map(v => v.replace(/"/g, '""'))
        .map(v => `"${v}"`)
        .join(',')

    // Combine headers and values into a CSV format
    const csv = `${headers}\n${values}`
    info('Created CSV', csv)

    // Assume the Vanguard role
    info('Setting up STS')
    const sts = new STS({
        apiVersion: '2011-06-15',
        region: 'us-east-1',
        accessKeyId: conf.AWS_ACCESS_KEY_ID,
        secretAccessKey: conf.AWS_SECRET_ACCESS_KEY,
    })

    info('Assuming vanguard role role', conf.VANGUARD_ROLE_ARN)
    const role = await sts.assumeRole({
        RoleArn: conf.VANGUARD_ROLE_ARN,
        RoleSessionName: 'vanguard',
        ExternalId: conf.VANGUARD_EXTERNAL_ID,
    }).promise()

    // Create an S3 handler with the assumed role
    info('Uploading to S3, assumed role key -> ', role.Credentials?.AccessKeyId)
    const s3 = new S3({
        apiVersion: '2006-03-01',
        region: 'us-east-1',
        credentials: {
            accessKeyId: role.Credentials?.AccessKeyId!,
            secretAccessKey: role.Credentials?.SecretAccessKey!,
            sessionToken: role.Credentials?.SessionToken,
        }
    })

    // Create the file name here for logging, they want like 
    // CURRENT/2024-10-02/2024-10-02-17-20-54_advisor.csv
    const fileName = 'CURRENT/' +
        new Date().toISOString().split('T')[0]! + '/' +
        vanguardTimestamp() +
        '_advisor.csv'
    console.info('Uploading to', fileName)

    // Post to Vanguard using the assumed role and their KMS
    const s3Resp = await s3.putObject({
        Bucket: conf.VANGUARD_BUCKET,
        Key: fileName,
        ContentType: 'binary',
        Body: Buffer.from(csv, 'binary'),
        ServerSideEncryption: 'aws:kms',
        SSEKMSKeyId: conf.VANGUARD_KMS_ARN,
        // Tagging: 'Application=ALP',
    }).promise()

    info('Uploaded to S3', s3Resp)
    return s3Resp
}