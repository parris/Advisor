export function parseConf(conf: string): any {
    const confObj: any = {}

    const confParts = conf.split('&')
    for (const part of confParts) {
        const [key, value] = part.split('=')
        confObj[key] = value
    }

    return confObj
}