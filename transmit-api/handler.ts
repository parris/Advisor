import { APIGatewayEvent } from 'aws-lambda'
import { ApiGatewayResponse, BadRequest, Success } from './ApiGatewayHelper'
import { post as vanguardPost, VanguardConf } from './vanguard'
import { parseConf } from './util'

import dotenv from 'dotenv'
import { error, info } from 'console'
import { sendSlackMessage } from './slack'
dotenv.config()

export interface AdvisorPost {
    Email: string
    FirstName: string
    LastName: string
    CompanyName: string
    Age: number
    Cash: number
    HHI: number
    LeadId: string
    Other: number
    CommunicationMethod: string
    BusinessOwner: boolean
    DoYouCurrentlyHaveAnAdvisor: boolean
    Married: boolean
    CampaignId: string
    EmpSponsoredRetire: number
    Investments: number
    MobilePhone: string
    NonEmpSponsoredRetire: number
    RealEstate: number
    RemoteLocal: string
    TimeTillRetire: string
    ZipCode: string
}

export async function handler(event: APIGatewayEvent): Promise<ApiGatewayResponse> {
    info('Received event', JSON.stringify(event))

    // Validate request
    if (!event.body) {
        return BadRequest('No body')
    }

    if (!event.pathParameters || !event.pathParameters['partnerKey']) {
        return BadRequest('No partner')
    }

    const partner = event.pathParameters['partnerKey']
    info('Transmitting to', partner)

    // Parse the request
    const advisorPost: AdvisorPost = JSON.parse(event.body)

    // Determine the post function and config
    let postFn
    let conf

    if (partner === 'vanguard') {
        // Parse ssm config
        const { AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY } = parseConf(process.env.VANGUARD_CONFIG!)

        // Parse env config
        conf = {
            AWS_ACCESS_KEY_ID: AWS_ACCESS_KEY_ID,
            AWS_SECRET_ACCESS_KEY: AWS_SECRET_ACCESS_KEY,
            VANGUARD_BUCKET: process.env.VANGUARD_BUCKET!,
            VANGUARD_EXTERNAL_ID: process.env.VANGUARD_EXTERNAL_ID!,
            VANGUARD_KMS_ARN: process.env.VANGUARD_KMS_ARN!,
            VANGUARD_ROLE_ARN: process.env.VANGUARD_ROLE_ARN!,
        } as VanguardConf

        postFn = vanguardPost
    } else {
        return BadRequest('Company not supported')
    }

    // Post the lead to a partner
    try {
        const resp = await postFn(advisorPost, conf)
        sendSlackMessage(`Successfully transmitted ${advisorPost.LeadId} to ${partner}`)
        return Success(resp)
    } catch (e) {
        error('Unhandled error', e)
        await sendSlackMessage(`Error transmitting to ${partner}: ${e.message}`)
        return BadRequest(e.message)
    }
}

// For local testing use the "Run current transmit-api TS file" debugger
if (process.env.IS_LOCAL) {
    handler({
        pathParameters: {
            partnerKey: 'vanguard'
        },
        body: JSON.stringify({
            "LeadId": "24274525-390b-43b5-a1ca-627f38eb3f47",
            "FirstName": "Taylor",
            "LastName": "Almy",
            "CommunicationMethod": null,
            "Email": "taylor_almy@yahoo.com",
            "MobilePhone": "2707020795",
            "DoYouCurrentlyHaveAnAdvisor": false,
            "ZipCode": "42301",
            "RemoteLocal": "Not Sure",
            "Age": "25",
            "TimeTillRetirement": "11+ Years",
            "BusinessOwner": false,
            "Married": false,
            "HHI": "50000",
            "Cash": "0",
            "EmpSponsoredRetire": "0",
            "NonEmpSponsoredRetire": "0",
            "Investments": "0",
            "RealEstate": "0",
            "Other": "2500000",
            "CampaignId": "3b054685-481f-4258-be55-6d7cca2f4fb8",
            "CampaignName": "Catchall Bid Floor for AUM > $250K"
        })
    } as unknown as APIGatewayEvent).then(console.log)
}