import { info } from "console"
import dotenv from 'dotenv'
dotenv.config()

const { WebClient } = require('@slack/web-api')
const slack = new WebClient(process.env.SLACK_TOKEN || 'xoxb-1791396330631-6438290431829-MC0OB6CsqmfmdylACOelF6Bc')

export const sendSlackMessage = async (text, channel = 'marketplace-leads-sold') => {
    info('Sending message to slack', text, channel)

    if (process.env.ENV !== 'prod') {
        channel = 'slackbot-test'
    }

    slack.chat.postMessage({ channel, text })
}
