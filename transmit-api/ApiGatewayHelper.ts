export interface ApiGatewayResponse {
    statusCode: number
    body: string
}

export function Success(body: any): ApiGatewayResponse {
    if (typeof body === 'object') {
        body = JSON.stringify(body)
    }

    return {
        statusCode: 200,
        body
    }
}

export function BadRequest(body: string): ApiGatewayResponse {
    return {
        statusCode: 400,
        body
    }
}