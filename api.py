import json

class Api:
    # Standardized return body format required
    # by AWS API gateway
    @staticmethod
    def lambda_resp(status_code, message, details = ""): 
        return_body = {
            "statusCode": status_code,
            "headers": {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*" ,
            },
            "body": json.dumps({
                "success": status_code < 400,
                "message": message,
                "details": details,
            })
        }

        print(return_body)
        return return_body

    # Standard redirect response
    @staticmethod
    def redirect(url):
        redirect_body = {
            "statusCode": 301,
            "headers": {
                "Location": url
            }
        }

        print("Redirecting")
        print(redirect_body)

        return redirect_body