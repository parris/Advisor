import requests
import re

from alert import Alerts

class Validation:
    def __init__(self):    
        self.base_url = "https://api.realvalidation.com/rpvWebService/"
        self.token = "090C21A7-5800-4605-865F-7835642B2CCE"
        self.alerts = Alerts()
        
    # Validate a phone number using realvalidation.com RealPhoneValidationFraudChk service
    # https://realphonevalidation.com/api-documentation/fraud-check-api-doc/
    # Returns a dictionary with interesting parameters
    def phone(self, num):
        print("Validating phone")

        # Setup API

        # Scrub phone to only digits
        phone_digits = re.sub(r'[^\d]+', '', num)

        # Call API
        url = "{0}/RealPhoneValidationFraudChk.php?phone={1}&token={2}".format(self.base_url, phone_digits, self.token)
        print(url)

        response = requests.get(url)
        print(response)

        # Don't blow up if fraud check fails
        if response.ok == False:
            self.alerts.email_alert("Failed phone validation\n{0}\n{1}\n".format(url, response.text))

            return {
                "error": True
            }

        # Parse API response
        data = response.text
        print(data)

        # Parse validation recommendation (can be "allow", "flag" "block")
        recommendation = get_simple_xml_value("recommendation", data)

        # Return parsed validation results
        return {
            "score":        get_simple_xml_value("score", data),
            "flag":         recommendation.lower() != "allow",
            "type":         get_simple_xml_value("phn_desc", data),
            "country":      get_simple_xml_value("location_country_name", data),
            "country_iso":  get_simple_xml_value("location_country_iso3", data),
        }


    # Validate a email using realvalidation.com rpvWebService/EmailVerify service
    # https://realphonevalidation.com/api-documentation/email-verification-api-doc/
    # Returns a dictionary with interesting parameters
    def email(self, email):
        print("Validating email")

        # Call API
        url = "{0}/EmailVerify.php?email={1}&token={2}".format(self.base_url, email, self.token)
        print(url)

        response = requests.get(url)
        print(response)

        # Don't blow up if email fraud check fails
        if response.ok == False:
            return {
                "error": True
            }

        # Parse API response
        data = response.text
        print(data)

        retval      = get_simple_xml_value("retval", data)
        status      = get_simple_xml_value("status", data)
        connected   = get_simple_xml_value("connected", data)
        disposable  = get_simple_xml_value("disposable", data)

        if retval != '0':
            self.alerts.email_alert("Failed email validation\n{0}\n{1}\n".format(url, data))

        # Return parsed/scrubbed response
        return {
            "error":        retval != '0',
            "flag":         status.lower() == "invalid" or disposable.lower() == "y",
            "status":       status,
            "error_code":   get_simple_xml_value("error_code", data),
            "connected":    connected,
            "disposable":   disposable,
        }

# Search for a value in an xml string like 
# <top-level>
#    <middle-level>The value</middle-level>
# </top-level>
# -> get_simple_xml_value(middle-value, xml) => "The value"
def get_simple_xml_value(value, xml):
    pat = r"\<{0}\>([^<]+)\<\/{0}\>".format(value)
    x = re.search(pat, xml)

    # Return None if pattern is not found
    if x == None:
        return ""

    # Return the first match if the pattern is found
    return x.group(1)