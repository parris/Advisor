import formstack
import json

event = {
    "body": json.dumps({
        "FormID": "5190882", 
        "UniqueID": "1080715144", 
        "First Name": "Parris",
        "Last Name": "Parris3", 
        "Email": "parris.parris3@gmail.com", 
        "Phone": "(704) 650-6754", 
        "What's your current total financial investments and savings?": "$250,000-$1,000,000", 
        "What's your annual household income? ": "$100,000 - $150,000", 
        "What is your age?": "18 to 24", 
        "How did you hear about us?": "Facebook", 
        "Which newsletter?": None, 
        "Who referred you?": None, 
        "URL": "https://advisor-v1.webflow.io/advisor-formstack-test"}) 
}

result = formstack.handler(event, "")