import os
os.environ["ENV"] = "preview"

from landing_page import pick_landing_page

test_data = {
 "id": "1078569555",
 "email": "parrris.sss@gmail.com",
 "Email": "parrris.sss@gmail.com",
 "First Name": "Parris",
 "FormID": "5190882",
 "How did you hear about us?": "Referral from Existing Client",
 "Last Name": "Formtest",
 "Phone": "(213) 456-7809",
 "UniqueID": "1078569555",
 "URL": "https://advisor-v1.webflow.io/join-formstack-test",
 "validation": {
  "email": {
   "connected": "N",
   "disposable": "N",
   "error": False,
   "error_code": "",
   "flag": False,
   "status": "valid"
  },
  "phone": {
   "country": "United States",
   "country_iso": "USA",
   "flag": False,
   "score": "303",
   "type": "FIXED_LINE"
  }
 },
 "What is your age?": "18 - 24",
 "What's your annual household income? ": "$75,000 - $99,999",
 "What's your current total financial investments and savings?": ">$5,000,000",
 "Which newsletter?": None,
 "Who referred you?": "Morning Brew"
}

picked = pick_landing_page(test_data)
print("Picked {0}".format(picked))