
import boto3
from botocore.exceptions import ClientError

class Alerts:
    def __init__(self):
        # Empty
        self.empty = True
        
        # Send an email alert
    #
    def email_alert(self, msg):
        # Create a new SES resource and specify a region.
        client = boto3.client('ses', region_name='us-east-1')

        # # For some reason this request needs to be made
        # # Every time we want to verify a new email address,
        # # even if it's added using the AWS web dashboard
        # response = client.verify_email_identity(
        #     EmailAddress="leanna@advisor.com"
        # )
        # print(response)

        # Send the email alert
        try:
            response = client.send_email(
                Destination={
                    'ToAddresses': [
                        "tech@advisor.com", "leanna@advisor.com",
                    ],
                },
                Message={
                    'Body': {
                        'Text': {
                            'Charset': "UTF-8",
                            'Data': msg,
                        },
                    },
                    'Subject': {
                        'Charset': "UTF-8",
                        'Data': "Alert from Advisor back end services",
                    },
                },
                Source="tech@advisor.com",
            )
        # Display an error if something goes wrong.	
        except ClientError as e:
            print("Error sending email:"),
            print(e.response['Error']['Message'])
        else:
            print("Email sent! Message ID:"),
            print(response['MessageId'])