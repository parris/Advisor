from dynamo import PixelRequests
pixel_requests = PixelRequests()

import urllib.parse
import os
import requests

advisor_base_url = "https://advisor-v1.webflow.io" if os.environ.get("ENV", "preview") == "preview" else  "https://advisor.com"

###
# Handles tracking pixel/post-back for leads that are qualified (which currently is
# defined as reaching the /schedule and /schedule-2 pages)
#
# This is intended to be called on the redirect from the Formstack webhook handler
#
def tracking_pixels(user):
    # Validate we have a URL saved for tracking
    url = user.get("URL", "")
    if url == "":
        print("No URL found for user")
        return
    else:
        print("Found URL: {}".format(url))

    # Do the tracking logic
    track_by_utm(user)

def track_by_utm(user):
    # Parse utm_source out of the URL
    url_parts = urllib.parse.urlparse(user.get("URL", ""))
    query = dict(urllib.parse.parse_qsl(url_parts.query))
    utm_source = query.get("utm_source")

    # If there is no utm_source, then we can't track so return
    if utm_source == None:
        print("No utm_source found in URL")
        return
    
    # Check for the various pixels
    pixel = None

    if utm_source == "financebuzz":
        pixel = "https://r.financebuzz.com/aff_lsr?transaction_id={}".format(query.get("clickid", ""))
    
    if utm_source == "gobankingrates":
        pixel = "https://products.gobankingrates.com/p/1546/{}.gif?ak={}".format(user.get("id", "LEADID"), query.get("campaignid", ""))

    if not pixel:
        print("No pixel found for utm_source: {}".format(utm_source))
        return
    
    try:
         # Call the pixel
        print("Calling pixel: {}".format(pixel))
        resp = requests.get(pixel)

        # Save the request/response
        pixel_requests.insert(utm_source, user.get("email"), pixel, resp.status_code)
        return
    except Exception as e:
        print("Error calling pixel: {}".format(e))
        return
