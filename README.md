# Advisor

## HTML Content

HTML files are intended to be put into Webflow as "HTML Embed" object.  They 
should be commented indicating where to put them

## Python Content

Python serverless lambda are intended to be deployed with the serverlessJs framework

```bash
# Deploy all services to preview
npm run deploy

# Deploy all services to prod
npm run deploy:prod
```

### Local Python

```
# Local python test scripts
# test-join.py calls the endpoint normally handled by AWS /join endpoint
python3 test-join.py
