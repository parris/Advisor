import json
import re
import urllib.parse
from dynamo import SignUps
from landing_page import pick_landing_page

from validate import Validation
from alert import Alerts
from api import Api
from mailchimp import MailChimp
from mailchimp_marketing.api_client import ApiClientError
   

#######
# Post lead handler, used for partners to post leads
# directly into the Advisor platform.
# 
# Does everything that the Formstack webhook handler does,
# but with a simpler spec.   
#    
def post_handler(event, _):
    print(event)
    data = json.loads(event["body"])
    print(data)

    alerts = Alerts()
    mailchimp = MailChimp()
    validation = Validation()

    # Define request body
    req_body = {
        "email":       data.get("email", "").lower(),                 # Always lowercase emails
        "phone":       re.sub(r'[^\d]+', '', data.get("phone", "")),  # Digits only in phone
        "first_name":  data.get("first_name", ""),
        "last_name":   data.get("last_name", ""),
        "age":         data.get("age", ""),
        "income":      data.get("income", ""),
        "investments": data.get("investments", ""),
        "sub1":        data.get("sub1", ""),
        "sub2":        data.get("sub2", ""),
        "sub3":        data.get("sub3", ""),
        "url":         "https://api.advisor.com" + event.get("path", ""),
    }

    if data.get("queryStringParameters") is not None:
        req_body["url"] += "&" + urllib.parse.urlencode(event.get("queryStringParameters", ""))

    print("Parsed request body:")
    print(req_body)

    # Validate
    validation_errors = []

    ## Email must contain @ and .
    if "@" not in req_body["email"] or "." not in req_body["email"]:
        validation_errors.append("Invalid email address")
    ## Email must be at least 5 chars
    if len(req_body["email"]) < len("a@bc.de"):
        validation_errors.append("Email too short")
    ## Phone cannot contain letters
    if re.match(r'[A-Za-z]', req_body["phone"]) is not None:
        validation_errors.append("Invalid phone number")
    ## Phone must be exactly 10 digits
    if len(req_body["phone"]) < 10 or len(req_body["phone"]) > 11:
        validation_errors.append("Incorrect phone number length")
    ## First name cannot be empty
    if req_body["first_name"] == None or len(req_body["first_name"]) == 0:
        validation_errors.append("First name cannot be empty")
    ## Last name cannot be empty
    if req_body["last_name"] == None or len(req_body["last_name"]) == 0:
        validation_errors.append("Last name cannot be empty")
    ## Age must be a number
    if not str(req_body["age"]).isnumeric():
        validation_errors.append("Age must be a number")
    ## Income must be a number
    if not str(req_body["income"]).isnumeric():
        validation_errors.append("Income must be a number")
    ## Investment must be a number
    if not str(req_body["investments"]).isnumeric():
        validation_errors.append("Investment must be a number")

    if len(validation_errors) > 0:
        return Api.lambda_resp(400, "validation error", validation_errors)

    # Return now for unit tests, below would need to be mocked
    if data.get("is_test") == True:
        return True
    else:
        print("test: " + str(event.get("is_test")))
    
    try:
        # Check phone/email validation API
        print("Checking phone/email validation")

        validation = Validation()
        req_body["validation"] = {
            "email": validation.email(req_body["email"]),
            "phone": validation.phone(req_body["phone"]),
        }
        print(req_body["validation"])

        # Save request to Dynamo
        print("saving to dynamo")
        SignUps().insert(req_body)

        # Call mailchimp
        print("saving to mailchimp")
        mailchimp.sign_up({
            "email": req_body["email"],
            "first-name": req_body["first_name"],
            "last-name": req_body["last_name"],
            "phone": req_body["phone"],
            "INCOME": req_body["income"],
            "INVESTMENT": req_body["investments"],
            "AGE": req_body["age"],
            "URL": req_body["url"],
            "other": {
                "sub1": req_body["sub1"],
                "sub2": req_body["sub2"],
                "sub3": req_body["sub3"],
            },
            "validation": req_body["validation"]
        })

        # Redirect to schedule page
        landing_page = pick_landing_page(req_body)
        print("landing page: " + landing_page)

        # Determine which service to return (15min/30min/none)
        service = "turn_down"

        if landing_page == "/schedule":
            service = "30_min"
        elif landing_page == "/schedule-2":
            service = "15_min"
   
        return Api.lambda_resp(200, service)
    except Exception as error:
        print("Error: {}".format(error.text))
        alerts.email_alert("Error in post lead: {0}\n{1}\n".format(error.text, data))
        return Api.lambda_resp(400, "error")