import uuid
import datetime
import boto3
from boto3.dynamodb.conditions import Key

######
# Interface/helper for the sign_ups collection in DynamoDB
#
class SignUps:
    def __init__(self):
        self.dynamodb = boto3.resource('dynamodb')

    # Add a new signup to the database
    def insert(self, user_data):
        try:
            # Create a primary key based on Formstack's UniqueID
            user_data["id"] = user_data.get("UniqueID", str(uuid.uuid4()))

            if user_data.get("email") == None:
                user_data["email"] = user_data.get("Email").lower()
            
            # Insert the table
            table = self.dynamodb.Table('sign_ups')
            table.put_item(Item=user_data)
            
        except Exception as err:
            print("Cannot save signup {0}".format(err))
    
    # Get a signup by email
    def get_user_by_email(self, email):
        try:
            table = self.dynamodb.Table('sign_ups')
            response = table.query(
                IndexName='email-index',
                KeyConditionExpression=Key('email').eq(email.lower())
            )
            return response["Items"][0]
        except Exception as err:
            print("Cannot get signup {0}".format(err))
            return None
        
######
# Interface/helper for the pixel_requests collection in DynamoDB
#
class PixelRequests:
    def __init__(self):
        self.dynamodb = boto3.resource('dynamodb')

    # Add a new signup to the database
    def insert(self, type, email, pixel_url, status):
        try:
            # Use email as primary key
            pixel_data = {
                "uuid": str(uuid.uuid4()),
                "type": type,
                "email": email.lower(),
                "pixel_url": pixel_url,
                "status": status,
                "created": str(datetime.datetime.now())
            }
            
            # Insert the table
            table = self.dynamodb.Table('pixel_requests')
            table.put_item(Item=pixel_data)
            
        except Exception as err:
            print("Cannot save pixel request {0}".format(err))
