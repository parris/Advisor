#!/bin/bash

# Usin bash (not fish) shell
# `source setup-venv.sh`

python3 -m venv venv
source venv/bin/activate

export AWS_PROFILE=advisor
export AWS_DEFAULT_REGION=us-east-1