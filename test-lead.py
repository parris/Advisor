import lead
import json

event = {
    "body": json.dumps({
        "email":       "parris.lead.test2@gmail.com",
        "phone":       "843-213-2234",
        "first_name":  "parris",
        "last_name":   "test-lead-2",
        "age":         30,
        "income":      100000,
        "investments": 100000,
        "sub1":        "sub1 test string",
        "sub2":        "2",
        "sub3":        "!@#$%",
    }) 
}

result = lead.post_handler(event, "")

print("Done")