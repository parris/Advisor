from api import Api
from dynamo import SignUps
from landing_page import pick_landing_page, is_conversion_page
from tracking import tracking_pixels

import time
import urllib.parse
import os

advisor_base_url = "https://advisor-v1.webflow.io" if os.environ.get("ENV", "preview") == "preview" else  "https://advisor.com"

###
# This is the handler configured as the redirect target for Formstack forms.  It waits for the Formstack webhook
# to complete, then redirects to the appropriate landing page.
#
def handle_signup(event, context):
    print("event")
    print(event)
    print("context")
    print(context)

    # Parse request
    query_string = event["queryStringParameters"]
    email = query_string["email"]

    # Redirect to root page if we can't find the email
    if email == "":
        return Api.redirect(advisor_base_url)
        
    try:
        # Load user from dynamo
        s = SignUps()
        user = s.get_user_by_email(email)

        # Give a few seconds for the Formstack webhook to fire,
        # polling ever 1 second for a max of 6 seconds
        if user == None:
            # Try 6 times to get from dynamo
            for _ in range(6):
                time.sleep(1)
                user = s.get_user_by_email(email)
                if user != None:
                    break

            # If still no user, then redirect to "thank you"
            if user == None:
                return Api.redirect("{0}/thank-you-we-will-call".format(advisor_base_url))

        # Redirect to schedule page
        landing_page = pick_landing_page(user)

        # Fire any tracking pixels
        if is_conversion_page(landing_page):
            tracking_pixels(user)
        else:
            print("Not a conversion page {}, skipping tracking pixels".format(landing_page))

        return Api.redirect("{0}{1}?".format(advisor_base_url, landing_page) + urllib.parse.urlencode({
            "email": email,
            "phone": user["Phone"],
            "fname": user["First Name"],
            "lname": user["Last Name"],
        }))
    except Exception as err:
        print("Error: {0}".format(err))
        return Api.redirect("{0}/thank-you-we-will-call".format(advisor_base_url))
