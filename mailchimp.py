import json
import hashlib
import re

import mailchimp_marketing as MailchimpMarketing
from boto3.dynamodb.conditions import Key

from alert import Alerts

#######
# Interface/helper for Mailchimp API
#
class MailChimp:
    def __init__(self):
        self.MAILCHIMP_LIST_ID = "0520777da8"

        # Configure Mailchimp API
        self.client = MailchimpMarketing.Client()
        self.client.set_config({
            "api_key": "6d1801a5c97a7ebddf5d05173fdbcb9e-us14",
            "server":  "us14"
        })

    def sign_up(self, user_data):
        # Create tags based on validation
        alerts = Alerts()
        list_tags = []

        print("Sending to mailchimp")

        if user_data["validation"]["phone"].get("flag") == True:
            list_tags.append("bad phone")

        if user_data["validation"]["email"].get("flag") == True:
            list_tags.append("bad email")

        if user_data["validation"]["phone"].get("error") == True:
            list_tags.append("phone validation failed")
            alerts.email_alert("Failed validation (phone)\n{0}\n".format(user_data))

        if user_data["validation"]["phone"].get("country_iso") != "USA":
            list_tags.append("intl phone")

        if  user_data["validation"]["email"].get("error") == True:
            list_tags.append("email validation failed") 
            alerts.email_alert("Failed validation (email)\n{0}\n".format(user_data))
   
        # Call mailchimp update API
        subscriber_hash = hashlib.md5(user_data["email"].encode("utf-8")).hexdigest()
        response = self.client.lists.set_list_member(self.MAILCHIMP_LIST_ID, subscriber_hash, {
            "email_address": user_data["email"],
            "skip_merge_validation": True,
            "status_if_new": "subscribed",
            "merge_fields": {
                "URL": user_data.get("URL", "https://advisor.com"),
                "FNAME": self.capitalize_name(user_data.get("first-name")),
                "LNAME": self.capitalize_name(user_data.get("last-name")),
                "AGE": user_data.get("AGE", "0"),
                "INCOME": user_data.get("INCOME", "0"),
                "INVESTMENT": user_data.get("INVESTMENT", "0"),
                "ABOUT_US": user_data.get("ABOUT_US", ""),
                "MMERGE9": self.format_phone(user_data.get("phone")),
                "VALID_RAW": json.dumps(user_data["validation"]),
                "OTHER": json.dumps(user_data.get("other")),
            },
            "tags": list_tags
        })
        
        print(response)
    
    def capitalize_name(self, name):
        for i in range(len(name)):
            if i == 0 or (name[i - 1] in [" ", "-", "'"]):
                replace_list = list(name)
                replace_list[i] = name[i].upper()
                name = "".join(replace_list)

        return name
    
    # Return phone format like "+1XXXXXXXXXX"
    def format_phone(self, phone):
        digits = re.sub(r"[^\d]", "", phone)

        if digits[0] == "1":
            digits = digits[1:]

        if len(digits) != 10:
            return ""

        return "+1" + digits