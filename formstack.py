import json
from dynamo import SignUps

from validate import Validation
from alert import Alerts
from api import Api
from mailchimp import MailChimp
from mailchimp_marketing.api_client import ApiClientError

#######
# Formstack webhook handler
# 
# This saves form data to Dynamo and sends it to Mailchimp
# 
# The Formstack form submit will wait for this process to complete
# before deciding which calendar to present to the user. 
# 
# Once the data is in Dynamo, the user can be redirected to the 
# correct calendar link.     
#    
def handler(event, _):
    print(event)
    data = json.loads(event["body"])
    print(data)

    alerts = Alerts()
    mailchimp = MailChimp()
    validation = Validation()
    
    # Mailchimp wants lowercase
    data["email"] = data.get("Email").lower()

    try:
        # Check phone/email validation API
        print("Checking phone/email validation")

        validation = Validation()
        data["validation"] = {
            "email": validation.email(data.get("email")),
            "phone": validation.phone(data.get("Phone")),
        }
        print(data["validation"])

        # Save request to Dynamo
        print("saving to dynamo")
        SignUps().insert(data)

        # Map request parameters to mailchimp parameters
        mailchimp_data = {
            "validation": data.get("validation"),
            "other": {}
        }

        for key in data:
            k = key.lower()

            if k == "email":
                mailchimp_data["email"] = data["email"]
            elif k == "first name":
                mailchimp_data["first-name"] = data[key]
            elif k == "last name":
                mailchimp_data["last-name"] = data[key]
            elif k == "phone":
                mailchimp_data["phone"] = data[key]
            elif k == "url":
                mailchimp_data["URL"] = data[key]
            elif "income" in k:
                mailchimp_data["INCOME"] = data[key]
            elif "investment" in k:
                mailchimp_data["INVESTMENT"] = data[key]
            elif "age" in k:
                mailchimp_data["AGE"] = data[key]
            elif "about us" in k:
                mailchimp_data["ABOUT_US"] = data[key]
            # Default to other, but skip "validation" since it's sent separately
            elif k != "validation": 
                mailchimp_data["other"][key] = data[key]

        # Call mailchimp
        mailchimp.sign_up(mailchimp_data)
   
        return Api.lambda_resp(200, "calendly")
    except ApiClientError as error:
        # Handle errors from Mailchimp
        print("Error calling mailchimp: {}".format(error.text))
        alerts.email_alert("Error calling mailchimp: {0}\n{1}\n".format(error.text, data))
        return Api.lambda_resp(400, "thank-you")