import json
import re

####
# Pick a landing page given user data and rules defined at ./landing_page_rules.json
#
# The rules spec is as follows
#
# 1. Identify the first rule labeled as "rule-start" 
# 2. Traverse the rules until a page URI is found
#    a. Match the question with a rule by picking the first word of the rule, and searching for
#       that word in each of the user input questions (inputs that contain a question mark)
#
#       ex: 
#         "income-investments-25k" will look for the word "income" in the user input questions, which
#         will match "What is your annual household income?"
#
#    b. If the user input is a range, then use the first number as the question value
#    c. Compare the user input, to the rules min/max values
#    d. If the input is within the min/max range, then the rule key is the next step
#    e. If the next step is a page URI (begins with "/"), then return that page URI, else look in 
#       the rules for the next step

###
# The entrypoint for this package
# 
def pick_landing_page(user):
    # Load calendar configuration
    f = open('landing_page_rules.json')
    rules = json.load(f)
    f.close()

    # Find the first rule indicated by "rule-start"
    start_rule = rules["rule-start"]

    # Traverse rules until we find a calendar
    return check_rules(start_rule, rules, user)

# Traverse rule set to find a page URI
def check_rules(rule, rules, data):
    rule_set = rules[rule]
    value = get_question_value(rule, data)

    print("Checking rule: {0} with value: {1}".format(rule, value))

    for rule_key in rule_set: 
        print("Rule key: {0}, rules: {1}".format(rule_key, rule_set[rule_key]))
        if value <= rule_set[rule_key]["max"] and value >= rule_set[rule_key]["min"]:
            if rule_key[0] == "/":
                return rule_key
            else:
                print("Recursing...")
                return check_rules(rule_key, rules, data)
    
    # Return to default page if no rules match
    return rules.get("default", "/schedule")


# Searches any questions (which end with ?) for the rule name
def get_question_value(rule_name, user):
    rule = rule_name.split("-")[0]
    print("rule -> '{}'".format(rule))

    for key in user:
        print("key --> '{}'".format(key))
        if rule == key:
            print("exact match found ---> '{}'".format(user[key]))
            return user[key]
        if "?" not in key:
            continue
        if rule in key:
            # Most question answers are dropdown ranges, so we need to parse the range
            print("parsing {}".format(user[key]))
            question_value = user[key]
            range_values   = re.split(r"(to|-)", question_value)[0]

            # And we need to strip out any non-numeric characters like $ or ,
            range_values = re.sub(r"[^\d.]", "", range_values)

            # And return as an integer for easy value comparisons
            return int(range_values)

# Checks if a page is configured as a "conversion event"
def is_conversion_page(page_uri):
    f = open('landing_page_rules.json')
    config = json.load(f)
    f.close()

    conversion_pages = config["conversion-pages"]

    return page_uri in conversion_pages