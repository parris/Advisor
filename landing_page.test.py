import os
os.environ["ENV"] = "preview"

import copy

from landing_page import pick_landing_page

base_test_data = {
 "id": "1078569555",
 "email": "parrris.sss@gmail.com",
 "Email": "parrris.sss@gmail.com",
 "First Name": "Parris",
 "FormID": "5190882",
 "How did you hear about us?": "Referral from Existing Client",
 "Last Name": "Formtest",
 "Phone": "(213) 456-7809",
 "UniqueID": "1078569555",
 "URL": "https://advisor-v1.webflow.io/join-formstack-test",
 "validation": {
  "email": {
   "connected": "N",
   "disposable": "N",
   "error": False,
   "error_code": "",
   "flag": False,
   "status": "valid"
  },
  "phone": {
   "country": "United States",
   "country_iso": "USA",
   "flag": False,
   "score": "303",
   "type": "FIXED_LINE"
  }
 },
 "What is your age?": "18 - 24",
 "What's your annual household income?": "$75,000 - $99,999",
 "What's your current total financial investments and savings?": ">$5,000,000",
 "Which newsletter?": None,
 "Who referred you?": "Morning Brew"
}

def test_log(age, income, investments):
    print("\n\nTESTING AGE:{0} INCOME:{1} INVESTMENTS:{2} ...........\n".format(age, income, investments))

def fail_log(age, income, investments, target):
    print("\nFAIL (AGE:{0} INCOME:{1} INVESTMENTS:{2}) GOT {3}".format(age, income, investments, target))

def success_log():
    print("\nSUCCESS")

# Test under 18 path
test_log("under 18", None, None)
under_18 = copy.deepcopy(base_test_data)
under_18["What is your age?"] = "17 or younger"
assert "/fit-under18" == pick_landing_page(under_18), fail_log(age, None, None, "/fit-under18")
success_log()

###
# TEST AGE / INCOME / INVESTMENT PATHS
###

# Test 18/100/250
age = "18"
income = "$100,000" 
investments = "$250,000"

test_log(age, income, investments)
path_test = copy.deepcopy(base_test_data)
path_test["What is your age?"] = age
path_test["What's your annual household income?"] = income
path_test["What's your current total financial investments and savings?"] = investments
lp = pick_landing_page(path_test)
assert "/schedule" == lp, fail_log(age, income, investments, lp)
success_log()

# Test 30/39/100 path
age = "64"
income = "$39,000" 
investments = "$250,000"

test_log(age, income, investments)
path_test = copy.deepcopy(base_test_data)
path_test["What is your age?"] = age
path_test["What's your annual household income?"] = income
path_test["What's your current total financial investments and savings?"] = investments
lp = pick_landing_page(path_test)
assert "/schedule" == lp, fail_log(age, income, investments, lp)
success_log()

# Test +5M path
age = "18"
income = "$250,000,000" 
investments = "$250,000,000"

test_log(age, income, investments)
path_test = copy.deepcopy(base_test_data)
path_test["What is your age?"] = age
path_test["What's your annual household income?"] = income
path_test["What's your current total financial investments and savings?"] = investments
lp = pick_landing_page(path_test)
assert "/schedule" == lp, fail_log(age, income, investments, lp)
success_log()

# Test 
age = "18"
income = "$99,999"
investments = "$249,000"

test_log(age, income, investments)
path_test = copy.deepcopy(base_test_data)
path_test["What is your age?"] = age
path_test["What's your annual household income?"] = income
path_test["What's your current total financial investments and savings?"] = investments
lp = pick_landing_page(path_test)
assert "/schedule-2" == lp, fail_log(age, income, investments, lp)
success_log()

# Test 
age = "18"
income = "$100,000"
investments = "$249,000"

test_log(age, income, investments)
path_test = copy.deepcopy(base_test_data)
path_test["What is your age?"] = age
path_test["What's your annual household income?"] = income
path_test["What's your current total financial investments and savings?"] = investments
lp = pick_landing_page(path_test)
assert "/schedule" == lp, fail_log(age, income, investments, lp)
success_log()

# Test 
age = "65"
income = None
investments = None

test_log(age, income, investments)
path_test = copy.deepcopy(base_test_data)
path_test["What is your age?"] = age
path_test["What's your annual household income?"] = income
path_test["What's your current total financial investments and savings?"] = investments
lp = pick_landing_page(path_test)
assert "/fit-over65" == lp, fail_log(age, income, investments, lp)
success_log()

# Test 
age = "18"
income = "$250,000"
investments = "$0"

test_log(age, income, investments)
path_test = copy.deepcopy(base_test_data)
path_test["What is your age?"] = age
path_test["What's your annual household income?"] = income
path_test["What's your current total financial investments and savings?"] = investments
lp = pick_landing_page(path_test)
assert "/schedule" == lp, fail_log(age, income, investments, lp)
success_log()

# Test 
age = "18"
income = "$50,000"
investments = "$150,000"

test_log(age, income, investments)
path_test = copy.deepcopy(base_test_data)
path_test["What is your age?"] = age
path_test["What's your annual household income?"] = income
path_test["What's your current total financial investments and savings?"] = investments
lp = pick_landing_page(path_test)
assert "/fit-financial-capacity" == lp, fail_log(age, income, investments, lp)
success_log()

# Test 
age = "25 to 34"
income = "$0-$40,000"
investments = "$0-25,000"

test_log(age, income, investments)
path_test = copy.deepcopy(base_test_data)
path_test["What is your age?"] = age
path_test["What's your annual household income?"] = income
path_test["What's your current total financial investments and savings?"] = investments
lp = pick_landing_page(path_test)
assert "/fit-financial-capacity" == lp, fail_log(age, income, investments, lp)
success_log()

# Test 
age = "25 to 34"
income = "$40,000-$75,000"
investments = "$25,000-$100,000"

test_log(age, income, investments)
path_test = copy.deepcopy(base_test_data)
path_test["What is your age?"] = age
path_test["What's your annual household income?"] = income
path_test["What's your current total financial investments and savings?"] = investments
lp = pick_landing_page(path_test)
assert "/fit-financial-capacity" == lp, fail_log(age, income, investments, lp)
success_log()

# Test 
age = "25 to 34"
income = "$40,000-$75,000"
investments = "$100,000-$250,000"

test_log(age, income, investments)
path_test = copy.deepcopy(base_test_data)
path_test["What is your age?"] = age
path_test["What's your annual household income?"] = income
path_test["What's your current total financial investments and savings?"] = investments
lp = pick_landing_page(path_test)
assert "/fit-financial-capacity" == lp, fail_log(age, income, investments, lp)
success_log()