import app
import json

event = {
    "body": json.dumps({
        "first-name": "test",
        "last-name": "phone",
        "email": "test.phone@gmail.com",
        "phone": "(843) 867-5309",
        "URL": "https://advisor-v1.webflow.io/join-validation-test"
    }) 
}

result = app.join_handler(event, "")